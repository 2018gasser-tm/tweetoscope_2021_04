import numpy as np
import kafka
import configparser
import json
import sys
import pickle


# Parse parameters inside parameter file param_file in argument.
def get_params(param_file):
    config = configparser.ConfigParser(strict=False)
    config.read(param_file)
    broker = config['kafka']['brokers']
    topic_in = config['topic']['cascade_properties']
    topic_samples = config['topic']['samples']
    topic_models = config['topic']['models']
    topic_alerts = config['topic']['alerts']
    topic_stats = config['topic']['stats']
    alpha = float(config['parameters']['alpha'])
    mu = float(config['parameters']['mu'])
    observations = config['times']['observation'].split(',')
    return broker,topic_in,topic_models,topic_samples,topic_alerts,topic_stats,alpha,mu,observations


# Computre the factor to correct the estimation of future retweets
def compute_factor(t_obs, n_supp, p, beta):
    global models, alpha, mu

    EM = mu * (alpha - 1) / (alpha - 2)
    n_star = p * EM
    G1 = n_supp * (1 - n_star)
    X = np.array([beta, n_star, G1]).reshape(1, -1)
    try:
        W = models[t_obs].predict(X)[0]
    except KeyError:
        #logger.info("the model for " + " T_obs= " + str(t_obs) + " is not received yet")
        W = 1
    return W
# Compute the estimated popularity of the tweet
def compute_n_est(n_obs, n_supp, W):
    return round(n_obs + W*n_supp)

# Produce empty string (serves as the key for the kafka message), the value of the message of type alert (map)
def produce_alert(t_obs, d_params, n_est):
    d = {"type":"alert", "cid":d_params["cid"], "msg":d_params["msg"], 
        "t_obs":t_obs, "n_tot":n_est}
    return d

# Return empty string (serves as the key for the kafka message), the value of the message of type stats (map)
def produce_stat(n_est, n_true, t_obs, cid):
    d = {"type":"stat", "cid":cid, 
        "t_obs":t_obs, "ARE":abs(n_est - n_true)/n_true}
    return d

# Return the key and value of the message to send to topic samples
def produce_sample(t_obs, cid, n_true, n_obs, n_supp, p, beta):
    global alpha, mu
    EM = mu * (alpha - 1) / (alpha - 2)
    n_star = p * EM
    G1 = n_supp * (1 - n_star)
    X = [beta, n_star, G1]
    W = (n_true - n_obs) / n_supp
    if W < 0 :
        #logger.critical("Encountered W < 0 (n_true < n_obs)")
        #print("Encountered W < 0 (n_true < n_obs)")
        return None,None
    d = {"type":"sample", "cid":cid, "X":X, "W": W}
    return str(t_obs), d


# Generate the message sent in the topic sample.
def process_msg(msg,producer,topic_alerts,topic_stats,topic_samples,observations):
    global data

    t_obs = int(msg.key.decode())
    msg = msg.value.decode()
    d = json.loads(msg.replace("\'", "\""))
    if d['type'] == 'parameters':
        n_obs = d['n_obs']
        n_supp = d['n_supp']
        W = compute_factor(t_obs, n_supp, d['params'][0], d['params'][1])
        n_est = compute_n_est(n_obs, n_supp, W)
        value = produce_alert(t_obs, d, n_est)
        producer.send(topic_alerts,value = value)
        try:
            _ = len(data[(t_obs, d['cid'])])
            data[(t_obs, d['cid'])] += [n_est, n_obs, n_supp, d['params'][0], d['params'][1]]
        except KeyError:
            data[(t_obs, d['cid'])] = [n_est, n_obs, n_supp, d['params'][0], d['params'][1]]
    
    else:
        n_true = d['n_tot']
        try:
            n = len(data[(t_obs, d['cid'])])
            data[(t_obs, d['cid'])] = [n_true] + data[(t_obs, d['cid'])]
        except KeyError:
            data[(t_obs, d['cid'])] = [n_true]
    
    if len(data[(t_obs, d['cid'])]) == 6:
        n_true = data[(t_obs, d['cid'])][0]
        n_est = data[(t_obs, d['cid'])][1]
        n_obs = data[(t_obs, d['cid'])][2]
        n_supp = data[(t_obs, d['cid'])][3]
        p = data[(t_obs, d['cid'])][4]
        beta = data[(t_obs, d['cid'])][5]
        value = produce_stat(n_est, n_true, t_obs, d['cid'])
        producer.send(topic_stats,value = value)
        key, value = produce_sample(t_obs, d['cid'], n_true, n_obs, n_supp, p, beta)
        if key!=None and value!=None:
            producer.send(topic_samples, key = str.encode(key), value = value)


# Return a map that store pairs (obs_window,model)
def update_models(msg):
    global models
    t_obs = int(msg.key.decode())
    model = pickle.loads(msg.value)
    models[t_obs] = model

if __name__ == "__main__" :
    # Global variable to store informations from messages of types params and size
    data = dict()  
    # Global variable to store the models                         
    models = dict()                        
    # parameters extracted from predictor.params
    broker,topic_in,topic_models,topic_samples,topic_alerts,topic_stats,alpha,mu,observations = get_params(sys.argv[1])
    # The properties of the consumer consuming from cascade_properties
    consumerProperties = { "bootstrap_servers":[broker],
                            "auto_offset_reset":"earliest", 
                            "group_id":"predictors", 
                            }
    # The properties of the consumer consuming from models
    consumerModels = { "bootstrap_servers":[broker],
                    "auto_offset_reset":"earliest", 
                    "group_id":"models", 
                    }
    # The kafka producer
    producer = kafka.KafkaProducer(bootstrap_servers = [broker], value_serializer=lambda v: json.dumps(v).encode('utf-8'))
    consumer1 = kafka.KafkaConsumer(**consumerProperties)
    consumer2 = kafka.KafkaConsumer(**consumerModels)
    consumer1.subscribe(topic_in)
    consumer2.subscribe(topic_models)
    
    try:
        # iterate over messages received from cascade_properties
        for msg in consumer1:
            # start by checking if a new model is received
            records = consumer2.poll()
            if records is not None: 
                for topicPartition, consumerRecords in records.items():
                    for record in consumerRecords:
                        #update the model
                        update_models(record)
            # process the message received from cascade_properties
            process_msg(msg,producer,topic_alerts,topic_stats,topic_samples,observations)
    finally:
        # Close consumer connection
        consumer1.close()
        consumer2.close()
    # Flush: force purging intermediate buffers before leaving
    producer.flush()
