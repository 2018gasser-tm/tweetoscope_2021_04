#!/bin/bash

docker run --rm -it --network host zookeeper 

docker run --rm -d --name kafka --network host \
           --env KAFKA_BROKER_ID=0 \
           --env KAFKA_LISTENERS=PLAINTEXT://:9092 \
           --env KAFKA_ZOOKEEPER_CONNECT=localhost:2181 \
           --env KAFKA_CREATE_TOPICS="tweets:4:1,cascade_series:1:1,cascade_properties:1:1"  \
           wurstmeister/kafka
           
docker build -t generator .

docker run --rm -it --network host generator
