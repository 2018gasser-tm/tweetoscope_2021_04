1-docker run --rm -it --network host zookeeper 


2- KAFKA :

docker run --rm -d --name kafka --network host \
           --env KAFKA_BROKER_ID=0 \
           --env KAFKA_LISTENERS=PLAINTEXT://:9092 \
           --env KAFKA_ZOOKEEPER_CONNECT=localhost:2181 \
           --env KAFKA_CREATE_TOPICS="tweets:4:1,cascade_series:1:1,cascade_properties:1:1"  \
           wurstmeister/kafka


4-docker build -t generator .
5-docker run --rm -it --network host generator

docker build -t collector .
docker run --rm -it --network host collector

docker build -t estimator .
docker run --rm -it --network host estimator

docker build -t predictor .
docker run --rm -it --network host predictor

docker build -t learner .
docker run --rm -it --network host learner
