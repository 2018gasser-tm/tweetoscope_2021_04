import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import json                       # To parse and dump JSON
from kafka import KafkaConsumer   # Import Kafka consumer
from kafka import KafkaProducer
import configparser
import scipy.optimize as optim

np.set_printoptions(precision = 2, suppress=True) # Default precision when displaying numpy arrays 
mpl.rcParams['figure.figsize'] = [ 16, 8 ] # Default figure size

# Parse parameters inside parameter file param_file in argument.
def get_params(param_file):
    config = configparser.ConfigParser(strict=False)
    config.read(param_file)
    broker = config['kafka']['brokers']
    topic_in = config['topic']['cascade_series']
    topic_out = config['topic']['cascade_properties']
    observations = config['times']['observation'].split(",")
    alpha = float(config['parameters']['alpha'])
    mu = float(config['parameters']['mu'])
    prior_params = config['parameters']['prior_params'].split(",")
    prior_params = [float(x) for x in prior_params]


    return broker,topic_in,topic_out,observations,alpha,mu,prior_params

def simulate_exp_hawkes_process(params, lambda0, max_size=2000):
    """
    Returns a 2D-array containing marked time points simulated from an exponential Hawkes process
    
    params   -- parameter tuple (kappa,beta) of the generating process
    lambda0  -- initial intensity at t = 0.
    max_size -- maximal authorized size of the cascade
    """
    
    kappa, beta = params    
    
    # The result is just a 1D array
    # However for further compatibility, we reserve a second colum for the magnitude of every point.
    # Every row thus describes a marked timepoint (ti, mi) with a magnitude set arbitrarily to one.

    # Create an unitialized array for optimization purpose (only memory allocation)
    T = np.empty((max_size,2))
    
    # Set magnitudes to 1.
    T[:,1] = 1.
    
    intensity = beta * lambda0
    t = 0.
    
    # Main loop
    for i in range(max_size):
        # Save the current point before generating the next one.
        T[i,0] = t
        
        # Sample inter-event time v from a homogeneous Poisson process
        u = np.random.uniform()
        v = -np.log(u)
        
        # Apply the inversion equation
        w = 1. - beta / intensity * v
        # Tests if process stops generating points.
        if w <= 0.:
            # Shrink T to remove unused rows
            T = T[:i,:]
            break
            
        # Otherwise computes the time jump dt and new time point t
        dt = - np.log(w) / beta
        t += dt
        
        # And update intensity accordingly
        intensity = intensity * np.exp(-beta * dt) + beta * kappa       
    return T

coef_Tmax = 1.1

def counting_process(cascade, T = None):
    """
    Returns a 2D-array N such that N(:,0) contains time samples t and N(:,1) contains images by point process N(t)
    
    cascade -- 2D-array containing samples of the point process as returned by simulate_exp_hawkes_process
    T       -- 1D array containing time samples whose value N(t) has to be computed (if None defines T to cover the full cascade)
    """
    
    tks = cascade[:,0]
    if T is None:
        Tmax = tks[-1] * coef_Tmax
        T = np.linspace(0,Tmax)
    N = np.zeros((len(T),2))
    N[:,0] = T
    for tk in tks:
        N[T >= tk,1] += 1
    return N

def neg_power_law(alpha, mu, size=1):
    """
    Returns a 1D-array of samples drawn from a negative power law distribution
    
    alpha -- power parameter of the power-law mark distribution
    mu    -- min value parameter of the power-law mark distribution
    size  -- number of samples
    """
    u = np.random.uniform(size=size)
    return mu * np.exp(np.log(u) / (1. - alpha))



def simulate_marked_exp_hawkes_process(params, m0, alpha, mu, max_size=10000):
    """
    Returns a 2D-array whose rows contain marked time points simulated from an exponential Hawkes process
    
    params   -- parameter tuple (p,beta) of the generating process
    m0       -- magnitude of the initial tweet at t = 0.
    alpha    -- power parameter of the power-law mark distribution
    mu       -- min value parameter of the power-law mark distribution
    max_size -- maximal authorized size of the cascade
    """
    
    p, beta = params    
    
    # Every row contains a marked time point (ti,mi).
    # Create an unitialized array for optimization purpose (only memory allocation)
    T = np.empty((max_size,2),dtype=float)
    
    intensity = beta * p * m0
    t, m = 0., m0
    
    # Main loop
    for i in range(max_size):
        # Save the current point before generating the next one.
        T[i] = (t,m)
        
        # Sample inter-event time v from a homogeneous Poisson process
        u = np.random.uniform()
        v = -np.log(u)
        
        # Apply the inversion equation
        w = 1. - beta / intensity * v
        # Tests if process stops generating points.
        if w <= 0.:
            T = T[:i,:]
            break
            
        # Otherwise computes the time jump dt and new time point t
        dt = - np.log(w) / beta
        t += dt
        
        # And update intensity accordingly
        m = neg_power_law(alpha, mu)
        lambda_plus = p * m
        intensity = intensity * np.exp(-beta * dt) + beta * lambda_plus        
    return T

def cond_intensity(params, history, T):
    """
    Returns a numpy 2D-array containing the conditional intensity of an exponential Hawkes process
    (first column is time, second is mapped intensity)
    
    params   -- parameter tuple (p,beta) of the Hawkes process
    history  -- (n,2) numpy array containing marked time points (t_i,m_i)
    T        -- 1D-array containing the input times for which the intensity must be computed
    """
    
    p, beta = params    
    I = np.zeros((len(T),2))
    I[:,0] = T
                 
    # For every marked point,
    for ti,mi in history:
        # Get all time indexes whose time is greater than ti
        J = T >= ti
        # Update the intensity for all times larger thanti
        I[J,1] += mi * np.exp(-beta * (T[J]-ti))
        
    # Don't forget to multiply by p*beta
    I[:,1] *= p * beta
    return I

def draw_intensity(params, history, Tmax = None, label = ""):
    """
    Draws an intensity plot along the history
    
    params   -- parameter tuple (p,beta) of the Hawkes process
    history  -- (n,2) numpy array containing marked time points (t_i,m_i)
    Tmax     -- upper bound of the plot interval
    label    -- legend label
    """

    if Tmax is None:
        Tmax = history[-1,0] * coef_Tmax
    T = np.linspace(-10., Tmax, 1000)
    I = cond_intensity(params, history, T)
    plt.plot(I[:,0] / 60., I[:,1] , label = label)
    plt.plot(history[:,0]/60, np.zeros(len(history)),'o', color='red')
    plt.title('Process intensity')
    plt.xlabel('Time (min)')

from scipy import integrate

def cumul_intensity(cond_intensity):
    """
    Returns a 2D array containing the cumulative intensity such that first column is time
    and second is mapped cumulative intensity up to given time.
 
    cond_intensity -- 2D-array as returned by cond_intensity function
    """
    
    T = cond_intensity[:,0]
    I = cond_intensity[:,1]
    
    C = np.empty_like(cond_intensity)
    C[:,0] = T
    C[:,1] = integrate.cumtrapz(I, T, initial=0)
    return C
    
def prediction(params, history, alpha, mu, t):
    """
    Returns the expected total numbers of points for a set of time points
    
    params   -- parameter tuple (p,beta) of the Hawkes process
    history  -- (n,2) numpy array containing marked time points (t_i,m_i)  
    alpha    -- power parameter of the power-law mark distribution
    mu       -- min value parameter of the power-law mark distribution
    t        -- current time (i.e end of observation window)
    """
    p,beta=params
    
    # We look for the index where ti<T_obs<ti+1. If history is correctly computed, i is the last index of the list. 
    j=0
    tj=history[j,0]
    while t>tj and j<len(history):
        j+=1
        if j==len(history):
            tj=history[len(history)-1,0]
        else:
            tj=history[j,0]

    n=j
    j=j-1
   
    n_etoile=p*mu*(alpha-1)/(alpha-2)
    somme=0
    for i in range(n):
        ti,mi=history[i]
        somme=somme+mi*np.exp(-beta*(t-ti))
    N=n+p*somme/(1-n_etoile)

    return(N)


def predictions(params, history, alpha, mu, T = None):
    """
    Returns the expected total numbers of points for a set of time points
    
    params   -- parameter tuple (p,beta) of the Hawkes process
    history  -- (n,2) numpy array containing marked time points (t_i,m_i)  
    alpha    -- power parameter of the power-law mark distribution
    mu       -- min value parameter of the power-law mark distribution
    T        -- 1D-array of times (i.e ends of observation window)
    """

    p,beta = params
    
    tis = history[:,0]
    if T is None:
        T = np.linspace(60,tis[-1],1000)

    N = np.zeros((len(T),2))
    N[:,0] = T
    
    EM = mu * (alpha - 1) / (alpha - 2)
    n_star = p * EM
    if n_star >= 1:
        raise Exception(f"Branching factor {n_star:.2f} greater than one")

    Si, ti_prev, i = 0., 0., 0
    
    for j,t in enumerate(T):
        for (ti,mi) in history[i:]:
            if ti >= t:
                break
            else:
                Si = Si * np.exp(-beta * (ti - ti_prev)) + mi
                ti_prev = ti
                i += 1

        n = i + 1
        G1 = p * Si * np.exp(-beta * (t - ti_prev))
        N[j,1] = n + G1 / (1. - n_star)
    return N

def naive_loglikelihood(params, history, t):
    """
    Returns the loglikelihood of a Hawkes process with exponential kernel
        
    params   -- parameter tuple (p,beta) of the Hawkes process
    history  -- (n,2) numpy array containing marked time points (t_i,m_i)  
    t        -- current time (i.e end of observation window)
    """
    
    p,beta = params    
    n = len(history)
    tis = history[:,0]
    mis = history[:,1]
    
    LL = (n-1) * np.log(p * beta)
    
    for i in range(1,n):      
        LL += np.log(np.sum(mis[:i] * np.exp(-beta * (tis[i] - tis[:i]))))
        
    LL -= p * np.sum(mis * (1. - np.exp(-beta * (t - tis))))

    return LL

def evaluate_likelihood_of_params(params, cascade, t, ratio = 0.7):
    n = len(cascade)
    print(f"Number of samples = {n}")
    print("Average likelihoods per sample:")
    def test_params(p):
        LL = naive_loglikelihood(p, cascade, t)
        print(f"  {np.exp(LL /n):0.7f} for (p,beta)=({p[0]:0.3f},{p[1]:0.5f})")
    
    test_params(params)
    test_params((params[0] * ratio, params[1]))
    test_params((params[0] / ratio, params[1]))
    test_params((params[0], params[1] * ratio))
    test_params((params[0], params[1] / ratio))

def loglikelihood(params, history, t):
    """
    Returns the loglikelihood of a Hawkes process with exponential kernel
    computed with a linear time complexity
        
    params   -- parameter tuple (p,beta) of the Hawkes process
    history  -- (n,2) numpy array containing marked time points (t_i,m_i)  
    t        -- current time (i.e end of observation window)
    """
    
    p,beta = params    
    
    if p <= 0 or p >= 1 or beta <= 0.: return -np.inf

    n = len(history)
    mis = history[:,1]
    
    LL = (n-1) * np.log(p * beta)
    logA = -np.inf
    prev_ti, prev_mi = history[0]
    
    i = 0
    for ti,mi in history[1:]:
        if(prev_mi + np.exp(logA) <= 0):
            print("Bad value", prev_mi + np.exp(logA))
        
        logA = np.log(prev_mi + np.exp(logA)) - beta * (ti - prev_ti)
        LL += logA
        prev_ti,prev_mi = ti,mi
        i += 1
        
    logA = np.log(prev_mi + np.exp(logA)) - beta * (t - prev_ti)
    LL -= p * (np.sum(mis) - np.exp(logA))

    return LL

def compute_MLE(history, t, alpha, mu,
                init_params=np.array([0.0001, 1./60]), 
                max_n_star = 1., display=False):
    """
    Returns the pair of the estimated loglikelihood and parameters (as a numpy array)

    history     -- (n,2) numpy array containing marked time points (t_i,m_i)  
    t           -- current time (i.e end of observation window)
    alpha       -- power parameter of the power-law mark distribution
    mu          -- min value parameter of the power-law mark distribution
    init_params -- initial values for the parameters (p,beta)
    max_n_star  -- maximum authorized value of the branching factor (defines the upper bound of p)
    display     -- verbose flag to display optimization iterations (see 'disp' options of optim.optimize)
    """
    
    # Define the target function to minimize as minus the loglikelihood
    target = lambda params : -loglikelihood(params, history, t)
    
    
    EM = mu * (alpha - 1) / (alpha - 2)
    eps = 1.E-8

    # Set realistic bounds on p and beta
    p_min, p_max       = eps, max_n_star/EM - eps
    beta_min, beta_max = 1/(3600. * 24 * 10), 1/(60. * 1)
    
    # Define the bounds on p (first column) and beta (second column)
    bounds = optim.Bounds(
        np.array([p_min, beta_min]),
        np.array([p_max, beta_max])
    )
    
    # Run the optimization
    res = optim.minimize(
        target, init_params,
        method='Powell',
        bounds=bounds,
        options={'xtol': 1e-8, 'disp': display}
    )
    
    # Returns the loglikelihood and found parameters
    return(-res.fun, res.x)


def compute_MAP(history, t, alpha, mu,max_n_star = 1, display=False):
    """
    Returns the expected total numbers of points for a set of time points
    
    params   -- parameter tuple (p,beta) of the Hawkes process
    history  -- (n,2) numpy array containing marked time points (t_i,m_i)  
    alpha    -- power parameter of the power-law mark distribution
    mu       -- min value parameter of the power-law mark distribution
    t        -- current time (i.e end of observation window)
    """
    # Compute prior moments
    mu_p, mu_beta, sig_p, sig_beta, corr = prior_params
    sample_mean = np.array([mu_p, mu_beta])
    cov_p_beta = corr * sig_p * sig_beta
    Q = np.array([[sig_p ** 2, cov_p_beta], [cov_p_beta, sig_beta **2]])
    # Apply method of moments
    cov_prior = np.log(Q / sample_mean.reshape((-1,1)) / sample_mean.reshape((1,-1)) + 1)
    mean_prior = np.log(sample_mean) - np.diag(cov_prior) / 2.

    # Compute the covariance inverse (precision matrix) once for all
    inv_cov_prior = np.asmatrix(cov_prior).I

    # Define the target function to minimize as minus the log of the a posteriori density    
    def target(params):
        log_params = np.log(params)
        
        if np.any(np.isnan(log_params)):
            return np.inf
        else:
            dparams = np.asmatrix(log_params - mean_prior)
            prior_term = float(- 1/2 * dparams * inv_cov_prior * dparams.T)
            logLL = loglikelihood(params, history, t)
            return - (prior_term + logLL)
      
    EM = mu * (alpha - 1) / (alpha - 2)
    eps = 1.E-8

    # Set realistic bounds on p and beta
    p_min, p_max       = eps, max_n_star/EM - eps
    beta_min, beta_max = 1/(3600. * 24 * 10), 1/(60. * 1)
    
    # Define the bounds on p (first column) and beta (second column)
    bounds = optim.Bounds(
        np.array([p_min, beta_min]),
        np.array([p_max, beta_max])
    )
    
    # Run the optimization
    res = optim.minimize(
        target, sample_mean,
        method='Powell',
        bounds=bounds,
        options={'xtol': 1e-8, 'disp': display}
    )
    # Returns the loglikelihood and found parameters
    return(-res.fun, res.x)


def prediction_from_estimator(estimator,history, alpha, mu, t, n_tries=1):
    """
    Compute the provided estimator for different observation windows and apply prediction according to it. Returns
    * the expected total numbers of points for a set of time points as a 1D-array
    * the computed loglikelihoods as a 1D-array
    * the estimated parameters as a 2D-array
    
    estimator -- function that implements an estimator that expect the same arguments as compute_MLE
    history   -- (n,2) numpy array containing marked time points (t_i,m_i)  
    alpha     -- power parameter of the power-law mark distribution
    mu        -- min value parameter of the power-law mark distribution
    T         -- 1D-array of times (i.e ends of observation window)
    n_tries   -- number of times the estimator is run. Best result is kept.
    """
    best_LL, best_params, best_N_sup = -np.inf,[], np.inf
    tis = history[:,0]
    partial_history = history[tis <= t]
    for j in range(n_tries):
        try:
            LL, param = estimator(partial_history, t, alpha, mu)
            if LL > best_LL:
                N_sup = prediction(param, partial_history, alpha, mu, t)
                best_LL, best_params, best_N_sup = LL, param, N_sup
        except:
            pass
    
    return best_LL, best_params, best_N_sup


#Create message a kafka message with a key containing the output data from the estimator.
def create_output_msg(message):
    
    # Parse message received and store it in a dictionnary
    d = eval(message.value.decode())
    # Store the key 
    T_obs = d["T_obs"]
    history = np.array(d["tweets"])
    # Set times to 0
    history[:,0] = history[:,0] - history[0][0]
    cid = d["cid"]
    msg = d['msg']
    # Compute number of observed tweets in a cascade
    n_obs = len(history[history[:,0]<T_obs])
    # Compute parameters and the prediction number of retweets
    LL, params, n_supp = prediction_from_estimator(compute_MAP,history, alpha, mu, T_obs, n_tries=2)
    T_obs = str(T_obs)
    if len(params)==2:
        params = params.tolist()
    d_result = {"type":"parameters","cid":cid,"msg":msg,"n_obs":n_obs,"n_supp":n_supp,"params":params}
    return T_obs,d_result


if __name__ == "__main__" :
    
    #Initialize parameter from input file.
    broker,topic_in,topic_out,observations,alpha,mu,prior_params = get_params(sys.argv[1])
    #Kafka consumer to collect latest messages from the data source, in our case the topic cascade_properties.
    consumerProperties = { "bootstrap_servers": [broker],
    "auto_offset_reset":"earliest",
    "group_id":"estimators" }
    consumer = KafkaConsumer(**consumerProperties)
    consumer.subscribe(topic_in)

    
    # Kafka producer to send messages to the topic out
    producer = KafkaProducer(bootstrap_servers = [broker],
     value_serializer=lambda v: json.dumps(v).encode('utf-8'),
     key_serializer=str.encode)
    for message in consumer:
        key,value = create_output_msg(message)
        # Test if the computation of key and value was successful
        if key!=None and value!=None:
            producer.send(topic_out, key = key, value = value)
    # Flush: empty intermediate buffers before leaving
    producer.flush()
