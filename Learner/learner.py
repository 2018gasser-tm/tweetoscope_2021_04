import configparser
import sys
import json
from sklearn.ensemble import RandomForestRegressor
import pickle
from kafka import KafkaProducer, KafkaConsumer
import configparser


# Parse parameters inside parameter file param_file in argument.
def get_params(param_file):
    config = configparser.ConfigParser(strict=False)
    config.read(param_file)
    broker = config['kafka']['brokers']
    topic_in = config['topic']['samples']
    topic_models = config['topic']['models']
    mini_batch = int(config['parameters']['mini_batch'])
    batch = int(config['parameters']['batch'])
    return broker,topic_in,topic_models

# Parameters extracted from learner.params
broker,topic_in,topic_models= get_params(sys.argv[1])


## Consumer of samples
    
consumer = KafkaConsumer(topic_in,bootstrap_servers = [broker],                        # List of brokers passed from the command line
    value_deserializer=lambda v: json.loads(v.decode('utf-8')),  # How to deserialize the value from a binary buffer
    auto_offset_reset="earliest",
    key_deserializer= lambda v: v.decode()                       # How to deserialize the key (if any)
)

## Producer of models
producer = KafkaProducer(
bootstrap_servers = [broker],
value_serializer=lambda v: pickle.dumps(v),
key_serializer=str.encode)                                           

X = []
y = []

for msg in consumer:        # Blocking call waiting for a new message
    t_obs = msg.key
    X.append(msg.value['X'])
    y.append(msg.value['W'])
    model =  RandomForestRegressor() ## random forest model
    model.fit(X,y)
    result = model
    # Send the message to the models topic
    producer.send(topic_models, key = str(t_obs), value = result)
# Flush: force purging intermediate buffers before leaving
producer.flush()