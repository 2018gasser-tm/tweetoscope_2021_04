
#include "tweetoscopeCollectorParams.hpp"
#include <iostream>
#include <iterator>
#include <cppkafka/cppkafka.h>
#include <tuple>

int main(int argc, char* argv[]) {

  if(argc != 2) {
    std::cout << "Usage : " << argv[0] << " <config-filename>" << std::endl;
    return 0;
  }

  tweetoscope::params::collector params(argv[1]);
  std::cout << std::endl
        << "Collector : " << std::endl
        << "----------"    << std::endl
        << std::endl
        << params << std::endl
        << std::endl;
  
  cppkafka::Configuration config = {
        { "metadata.broker.list", params.kafka.brokers },
        { "group.id", 1 },
    };
  cppkafka::Consumer consumer(config);
  consumer.subscribe({params.topic.out_series});

  while (true) {
     auto msg = consumer.poll();
      if( msg && ! msg.get_error() ) {
        auto istr = std::string(msg.get_payload());
        std::cout << "msg :"<<istr <<std::endl;
      }
    }

return 0;
}


