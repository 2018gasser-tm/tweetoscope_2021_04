
#include "tweetoscopeCollectorParams.hpp"
#include <iostream>
#include <iterator>
#include <cppkafka/cppkafka.h>
#include <tuple>

int main(int argc, char* argv[]) {

  if(argc != 2) {
    std::cout << "Usage : " << argv[0] << " <config-filename>" << std::endl;
    return 0;
  }

  tweetoscope::params::collector params(argv[1]);
  std::cout << std::endl
        << "Collector : " << std::endl
        << "----------"    << std::endl
        << std::endl
        << params << std::endl
        << std::endl;

  cppkafka::Configuration config = {
        { "metadata.broker.list", params.kafka.brokers },
        { "group.id", 1 },
    };
  cppkafka::Consumer consumer(config);
  consumer.subscribe({params.topic.in});

  //Create a Processor Map 
  tweetoscope::Processors processors(argv[1]);

  while (true) {
     auto msg = consumer.poll();
      if( msg && ! msg.get_error() ) {
        tweetoscope::tweet twt;
        auto key = tweetoscope::cascade::idf(std::stoi(msg.get_key()));
        auto istr = std::istringstream(std::string(msg.get_payload()));
        istr >> twt;
        twt.source = key;
        processors += twt;
        consumer.commit(msg);
      }
    }

return 0;
}


