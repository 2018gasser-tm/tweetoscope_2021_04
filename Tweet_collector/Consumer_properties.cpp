
#include "tweetoscopeCollectorParams.hpp"
#include <iostream>
#include <iterator>
#include <cppkafka/cppkafka.h>
#include <tuple>

int main(int argc, char* argv[]) {

  if(argc != 2) {
    std::cout << "Usage : " << argv[0] << " <config-filename>" << std::endl;
    return 0;
  }

  tweetoscope::params::collector params(argv[1]);
  std::cout << std::endl
        << "Collector : " << std::endl
        << "----------"    << std::endl
        << std::endl
        << params << std::endl
        << std::endl;
  
  /*
  // Brokers
  auto broker = params.kafka.brokers;
  // Topics
  auto in = params.topic.in; //Topic en entrée : Tweets
	auto out_series = params.topic.out_series; //Topic en sortie : cascade_series
	auto out_properties= params.topic.out_properties; //Topic en sortie : cascade_properties
  //Temps
  auto observation = params.times.observation;
  auto terminated = params.times.terminated;
  //Cascade
	auto min_cascade_size= params.cascade.min_cascade_size; //   min_cascade_size
  */
  cppkafka::Configuration config = {
        { "metadata.broker.list", params.kafka.brokers },
        { "group.id", 1 },
    };
  cppkafka::Consumer consumer(config);
  consumer.subscribe({params.topic.out_properties});

  while (true) {
     auto msg = consumer.poll();
      if( msg && ! msg.get_error() ) {
        auto key = tweetoscope::cascade::idf(std::stoi(msg.get_key()));
        auto istr = std::string(msg.get_payload());
        std::cout << "key : " << key << "  msg :"<<istr <<std::endl;
      }
    }

return 0;
}


