/*

  The boost library has clever tools for handling program
  parameters. Here, for the sake of code simplification, we use a
  custom class.

*/


#pragma once

#include <tuple>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstddef>
#include <stdexcept>
#include <vector>
#include <string>
#include <memory>
#include <boost/heap/binomial_heap.hpp>
#include <cppkafka/cppkafka.h>
#include <queue>
#include <map>
#include <utility>

namespace tweetoscope {

  struct tweet;
  struct Cascade;
  struct Processor;
  struct Processors;

  namespace params {
    namespace section {
      struct Kafka {
	std::string brokers;
      };
      
      struct Topic {
	std::string in, out_series, out_properties;
      };
      
      struct Times {
	std::vector<std::size_t> observation;
	std::size_t              terminated;
      };

      struct Cascade {
	std::size_t min_cascade_size;
      };
    }
    
      
    struct collector {
    private:
      std::string current_section;

      std::pair<std::string, std::string> parse_value(std::istream& is) {
	char c;
	std::string buf;
	is >> std::ws >> c;
	while(c == '#' || c == '[') {
	  if(c == '[') std::getline(is, current_section, ']');
	  std::getline(is, buf, '\n');
	  is >> std::ws >> c;
	}
	is.putback(c);
	std::string key, val;
	is >> std::ws;
	std::getline(is, key, '=');
	is >> val;
	std::getline(is, buf, '\n');
	return {key, val};
      }
      
    public:

      section::Kafka   kafka;
      section::Topic   topic;
      section::Times   times;
      section::Cascade cascade;

      collector(const std::string& config_filename) {
	std::ifstream ifs(config_filename.c_str());
	if(!ifs)
	  throw std::runtime_error(std::string("Cannot open \"") + config_filename + "\" for reading parameters.");
	ifs.exceptions(std::ios::failbit | std::ios::badbit | std::ios::eofbit);
	try {
	  while(true) {
	    auto [key, val] = parse_value(ifs);
	    if(current_section == "kafka") {
	      if(key == "brokers") kafka.brokers = val;
	    }
	    else if(current_section == "topic") {
	      if     (key == "in")             topic.in             = val;
	      else if(key == "out_series")     topic.out_series     = val;
	      else if(key == "out_properties") topic.out_properties = val;
	    }
	    else if(current_section == "times") {
	      if     (key == "observation")    times.observation.push_back(std::stoul(val));
	      else if(key == "terminated")     times.terminated = std::stoul(val);
	    }
	    else if(current_section == "cascade") {
	      if (key == "min_cascade_size")   cascade.min_cascade_size = std::stoul(val);
	    }
	  }
	}
	catch(const std::exception& e) {/* nope, end of file occurred. */}
      }
    };

    inline std::ostream& operator<<(std::ostream& os, const collector& c) {
      os << "[kafka]" << std::endl
	 << "  brokers=" << c.kafka.brokers << std::endl
	 << std::endl
	 << "[topic]" << std::endl
	 << "  in=" << c.topic.in << std::endl
	 << "  out_series=" << c.topic.out_series << std::endl
	 << "  out_properties=" << c.topic.out_properties << std::endl
	 << std::endl
	 << "[times]" << std::endl;
      for(auto& o : c.times.observation)
	os << "  observation=" << o << std::endl;
      os << "  terminated=" << c.times.terminated << std::endl
	 << std::endl
	 << "[cascade]" << std::endl
	 << "  min_cascade_size=" << c.cascade.min_cascade_size << std::endl;
	  return os;
    }
  }

  using timestamp = std::size_t;
  namespace source {
    using idf = std::size_t;
  }
  namespace cascade {
    using ref_c = std::shared_ptr<Cascade>;
    using ref_w = std::weak_ptr<Cascade>; 
    using idf = std::size_t;
  }

  struct tweet {
    std::string type = "";
    std::string msg  = "";
    timestamp time   = 0;
    double magnitude = 0;
    tweetoscope::source::idf tid = 0;
    std::string info = "";
    source::idf source = 0;
  };

  inline std::string get_string_val(std::istream& is) {
    char c;
    is >> c; // eats  "
    std::string value;
    std::getline(is, value, '"'); // eats tweet", but value has tweet
    return value; 
  }

  inline std::istream& operator>>(std::istream& is, tweet& t) {
    // A tweet is  : {"type" : "tweet"|"retweet", 
    //                "msg": "...", 
    //                "time": timestamp,
    //                "magnitude": 1085.0,
    //                "source": 0,
    //                "info": "blabla"}
    std::string buf;
    char c;
    is >> c; // eats '{'
    is >> c; // eats '"'
    while(c != '}') { 
      std::string tag;
      std::getline(is, tag, '"'); // Eats until next ", that is eaten but not stored into tag.
      is >> c;  // eats ":"
      if     (tag == "type")    t.type = get_string_val(is);
      else if(tag == "msg")     t.msg  = get_string_val(is);
      else if(tag == "info")    t.info = get_string_val(is);
      else if(tag == "t")       is >> t.time;
      else if(tag == "m")       is >> t.magnitude;
      else if(tag == "tweet_id")  is >> t.tid;
            
      is >> c; // eats either } or ,
      if(c == ',')
        is >> c; // eats '"'
    } 
    return is;
  }

// This is the comparison functor for boost queues.
struct cascade_ref_comparator {
  bool operator()(cascade::ref_c op1, cascade::ref_c op2) const;
};

  // We define our queue type.
  using priority_queue = boost::heap::binomial_heap<cascade::ref_c, boost::heap::compare<cascade_ref_comparator>>;

  struct Cascade {

    tweetoscope::source::idf tweet_id;   // Tweet id
    std::string info; // Cascade id
    std::string msg; // Message of the tweet
    tweetoscope::timestamp last_t;    /// Time of the newest retweet
    tweetoscope::timestamp first_t;     // Time of the first tweet
    std::vector<std::pair<tweetoscope::timestamp,double>> tweets; // Tweet cracteristics time and magnitude
    priority_queue::handle_type location; // This is "where" the element

    /**
     * @brief Default Constructor of Cascade object
     * 
     */
    Cascade() = default;

    /**
     * @brief Destroy the Cascade object
     * 
     */
    virtual ~Cascade() {};
    
    Cascade(const tweet& twt) : tweet_id(twt.tid), info(twt.info), msg(twt.msg), last_t(twt.time),first_t(twt.time){
      tweets.push_back({twt.time,twt.magnitude});
    };
    
    inline bool operator<(const Cascade& other) const {return last_t < other.last_t;}

    void operator+=(const tweet& other) {
    last_t = other.time;
    tweets.push_back(std::make_pair(other.time,other.magnitude));}
    
    cascade::ref_c make_cascade(const tweet& twt) {
      return std::make_shared<Cascade>(twt);}
  };

  inline std::ostream& operator<<(std::ostream& os, const Cascade& cas) {
      os  << "  tweet_id= " << cas.tweet_id << "  "
	 << "  cid= " << cas.info << " , " << std::endl
   << "  msg= " << cas.msg << " , " << std::endl;
   os << " last_tweet_time =" << cas.last_t << " , " << std::endl;
	 os << " tweets = {[";
   for(auto it = cas.tweets.begin(); it != cas.tweets.end(); ++it){
        os << "(" << it->first << ", " << it->second << ")";
        if (it != cas.tweets.end()-1) os << ",";
        }
        os << "]}";
	  return os;
    }

    bool cascade_ref_comparator::operator()(cascade::ref_c op1, cascade::ref_c op2) const {return *op2 < *op1;};

struct Processor {
    params::collector parameters;
    tweetoscope::priority_queue cascade_queue; // Priority_queue to manage cascade termination
    std::map<timestamp, std::queue<cascade::ref_w>> partial_cascade; // Map for managing partial cascades
    std::map<cascade::idf, cascade::ref_w> symbols; // Map to facilitate finding cascade reference for its id
    /**
     * @brief Default constructor of Processor object
     * 
     */
    Processor() = default;

    Processor(const tweet& twt, params::collector p);

     /**
     * @brief Destroy the Processor object
     * 
     */
    virtual ~Processor() {};

};

    Processor::Processor(const tweet& twt, params::collector p) : parameters(p) {
    Cascade cascade;
    auto r = cascade.make_cascade(twt);
    r->location = cascade_queue.push(r);
    for (auto obs : p.times.observation) {
      partial_cascade.insert({obs, {}});
      partial_cascade[obs].push(r);
    }
    symbols.insert(std::make_pair(twt.tid, r));
    }

        std::string format_cascade_series(const Cascade& c, timestamp obs) {
        std::ostringstream os;
        os << "{\"type\" : "  << "\"serie\""  << " , "
        << "\"cid\" : "    << c.tweet_id        << " , "
        << "\"msg\" : "    << c.msg        << " , "
        << "\"T_obs\" : "  << obs          << " , " 
        << "\"tweets\" : [";
        for(auto ptr_t = c.tweets.begin(); ptr_t != c.tweets.end(); ++ptr_t){
        os << "(" << ptr_t->first << ", " << ptr_t->second << ")";
        if (ptr_t != c.tweets.end()-1) os << ",";
        }
        os << "]}";
        return os.str();
    }

    std::string format_cascade_properties(const Cascade& c) {
        std::ostringstream os;
        os << "{\"type\" : "  << "\"size\""    << " , "
        << "\"cid\" : "    << c.tweet_id        << " , "
        << "\"n_tot\" : "  << c.tweets.size() << " , "
        << "\"t_end\" : "  << c.last_t << "}";
        return os.str();
    }

inline void send_kafka(cascade::ref_c c_ptr,Processors& pr, timestamp obs, bool Terminated);

struct Processors {
    
    std::map<source::idf, Processor> processors; // A map to store all processors
    params::collector parameters; // Parsed parameters
    cppkafka::Configuration config; // Configuration of the producer to send messages
    cppkafka::Producer  producer; // Kafka Producer 

    inline Processors() = default;

    inline Processors(const std::string& config_filename) : parameters(config_filename),
    config({{"metadata.broker.list", parameters.kafka.brokers},
      {"log.connection.close", false }}),producer(config) {};
    /**
     * @brief Construct Processors object from a parameters collector object
     * This constuctor is mainly used for unit tests
     * @param p 
     */
    inline Processors(const params::collector& p) : parameters(p),config({{"metadata.broker.list", parameters.kafka.brokers},
      {"log.connection.close", false }}),producer(config) {};
    /**
     * @brief Destroy the Processors object
     * 
     */
    inline virtual ~Processors() {};

    void operator+=(const tweet& twt);
};


void send_kafka(cascade::ref_c c_ptr, Processors& pr, tweetoscope::timestamp obs, bool Terminated){
      if (Terminated) { 
        cppkafka::MessageBuilder builder_p {pr.parameters.topic.out_properties};
        auto key = std::to_string(obs);
        builder_p.key(key);
        auto msg = format_cascade_properties(*c_ptr);
        builder_p.payload(msg);
        pr.producer.produce(builder_p);
      } else {
        cppkafka::MessageBuilder builder_s {pr.parameters.topic.out_series};
        auto msg = format_cascade_series(*c_ptr,obs);
        builder_s.payload(msg);
        pr.producer.produce(builder_s);
      }
    }

      void Processors::operator+=(const tweet& twt) {
        Cascade cascade;
        //If the source does not exist, Construct a Processor and insert it in the map
        auto [p_ptr,is_new_source] = processors.try_emplace(twt.tid,twt,this->parameters);
        // Case 2 : Source exists
        if (!is_new_source) {
          auto c_ptr = cascade.make_cascade(twt);
          // Check if the cascade id is already in the symbol table, if not we insert it 
          auto [it_s, is_symbol_created] = p_ptr->second.symbols.insert(std::make_pair(twt.tid, c_ptr));

          // Partial cascades 
          for(auto& [obs, cascades]: p_ptr->second.partial_cascade){
            while(!cascades.empty()) {
              if (auto sp = cascades.front().lock()) {
                if (twt.time - sp->first_t >= obs) {
                  // send the kafka : topic = cascade_series
                  // Check if cascade lenght >= min_cascade_size
                  if (sp->tweets.size()>=parameters.cascade.min_cascade_size){
                      send_kafka(sp, *this, obs, false);
                  }
                  cascades.pop();
                } else break; //Since by construction of the queue oldest cascades are in the front, Break as soon as a cascade does not satisfy the condition
              } else cascades.pop(); //Delete cascades which are terminated but the weak_pointer is still present in the partial_cascade map
            }
            // Newly created cascade, so it should be added to all the partial cascades
            if(is_symbol_created) cascades.push(c_ptr);
          }

          //Queue
          // Test if cascades in the queue verify the condition of termination, Send the kafka message and erase the terminated cascades from the queue
          while(!p_ptr->second.cascade_queue.empty() && this->parameters.times.terminated \
               <= twt.time - p_ptr->second.cascade_queue.top()->last_t) {
            
            auto r = p_ptr->second.cascade_queue.top();
            // send the kafka : topic = cascade_properties
            for (auto obs : this->parameters.times.observation) {
                // Check if cascade lenght >= min_cascade_size
                if (r->tweets.size() >= parameters.cascade.min_cascade_size){
                  send_kafka(r, *this, obs, true);
                }
            }
            p_ptr->second.cascade_queue.pop();
          }
        // If the cascade is newly created, push it to the queue
        if (is_symbol_created) c_ptr->location = p_ptr->second.cascade_queue.push(c_ptr);

        //Update queue
        if(auto sp = it_s->second.lock()) {
          // Update the latest time of the cascade
          sp->last_t = twt.time;
          // Only push the tweet if the cascade is not newly created
          if (!is_symbol_created) sp->tweets.push_back(std::make_pair(twt.time,twt.magnitude)); // Push the tweets to the cascade
          // Update the location in the queue (when tweet is added the priority of the cascade changes)
          p_ptr->second.cascade_queue.update(sp->location);
        }
        }

    }
}
